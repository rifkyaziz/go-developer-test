package models

import "errors"

var (
	ErrorProductNotFound = errors.New("product not found")
	ErrorProductStockNotAvailable = errors.New("product stock not available")
)
