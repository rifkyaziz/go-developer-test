package usecase

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/rifkyaziz/go-developer-test/models"
	"gitlab.com/rifkyaziz/go-developer-test/service"
	"golang.org/x/crypto/bcrypt"
)

type userUseCase struct {
	userRepo service.UserRepository
	config map[string]string
}

func NewUserUseCase(repo service.UserRepository, config map[string]string) service.UserUseCase {
	return userUseCase{repo, config}
}

func (u userUseCase) CreateUser(params models.UserRequest) (result models.User, err error) {
	hashedPwd, err  := u.generatePassword(params.Password)
	if err != nil {
		return result, err
	}

	params.Password = hashedPwd
	return u.userRepo.CreateUser(params)
}

func (u userUseCase) FindUser(userID int64) (result models.User, err error) {
	return u.userRepo.FindUser(userID)
}

func (u userUseCase) ListUser() (result []models.User, err error) {
	return u.userRepo.ListUser()
}

func (u userUseCase) UpdateUser(userID int64, params models.UserRequest) (err error) {
	user, err := u.userRepo.FindUser(userID)
	if err != nil {
		return err
	}

	if user.ID == 0 {
		msg := fmt.Sprintf("user id %d is not available", userID)
		return errors.New(msg)
	}

	hashedPwd, err  := u.generatePassword(params.Password)
	if err != nil {
		return err
	}

	params.Password = hashedPwd
	return u.userRepo.UpdateUser(userID, params)
}

func (u userUseCase) DeleteUser(userID int64) (err error) {
	return u.userRepo.DeleteUser(userID)
}

func (u userUseCase) Login(params models.AuthRequest) (result models.AuthResponse, err error) {
	user, err := u.userRepo.FindUserByUsername(params.Username)

	if err != nil {
		return result, err
	}

	if u.comparePassword(user.Password, params.Password) {
		return result, errors.New("password did'nt match")
	}

	method := jwt.GetSigningMethod("HS256")
	mapClaims := jwt.MapClaims{}
	mapClaims["id"] = user.ID
	mapClaims["username"] = user.Username
	mapClaims["fullname"] = user.FullName

	accessTokenClaim := jwt.NewWithClaims(method, mapClaims)
	accessToken, err := accessTokenClaim.SignedString([]byte(u.config["JWT_SECRET_KEY"]))
	if err != nil {
		return result, err
	}
	refreshTokenClaim := jwt.NewWithClaims(method, mapClaims)
	refreshToken, err := refreshTokenClaim.SignedString([]byte(u.config["JWT_SECRET_KEY"]))
	if err != nil {
		return result, err
	}

	result.AccessToken = accessToken
	result.RefreshToken= refreshToken

	return result, nil
}

func (u userUseCase) generatePassword(plainPassword string) (result string, err error) {
	pwd, err := bcrypt.GenerateFromPassword([]byte(plainPassword), bcrypt.DefaultCost)
	if err != nil {
		return result, errors.New("failed to generate password")
	}
	result = string(pwd)
	return result, nil
}

func (u userUseCase) comparePassword(hashedPassword, plainPassword string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(plainPassword)); err != nil {
		return false
	}
	return true
}