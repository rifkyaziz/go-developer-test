package main

import (
	"math"
)

//func main() {
//	r, p := solution4([]int{-6,4,-5,8,-10,0,8})
//	fmt.Println(r)
//	fmt.Println(p)
//}

func solution4(arr []int) (result []int, product int) {
	var (
		minPrev int
		maxPrev int
	)
	for i := range arr {
		currMax := math.Max(float64(arr[i]*maxPrev), float64(arr[i]*(minPrev)))
		currMin := math.Min(float64(arr[i]*maxPrev), float64(arr[i]*(minPrev)))

		maxPrev = int(math.Max(float64(arr[i]), currMax))
		minPrev = int(math.Min(float64(arr[i]), currMin))

		if maxPrev > product {
			product = maxPrev
			result = append(result, arr[i])
		}
	}

	return result, product
}