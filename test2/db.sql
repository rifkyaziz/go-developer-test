--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7
-- Dumped by pg_dump version 11.7

-- Started on 2020-08-03 23:07:08 WIB

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3957 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 201 (class 1259 OID 32893)
-- Name: payment_methods; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment_methods (
    method_id bigint NOT NULL,
    provider character varying
);


ALTER TABLE public.payment_methods OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 32891)
-- Name: payment_method_method_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.payment_method_method_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_method_method_id_seq OWNER TO postgres;

--
-- TOC entry 3958 (class 0 OID 0)
-- Dependencies: 200
-- Name: payment_method_method_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.payment_method_method_id_seq OWNED BY public.payment_methods.method_id;


--
-- TOC entry 205 (class 1259 OID 32938)
-- Name: payment_method_profiles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment_method_profiles (
    profile_id bigint NOT NULL,
    user_id bigint,
    method_id bigint,
    account_no character varying
);


ALTER TABLE public.payment_method_profiles OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 32936)
-- Name: payment_method_profiles_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.payment_method_profiles_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_method_profiles_profile_id_seq OWNER TO postgres;

--
-- TOC entry 3959 (class 0 OID 0)
-- Dependencies: 204
-- Name: payment_method_profiles_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.payment_method_profiles_profile_id_seq OWNED BY public.payment_method_profiles.profile_id;


--
-- TOC entry 202 (class 1259 OID 32907)
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products (
    product_id bigint NOT NULL,
    price numeric(50,3),
    name character varying NOT NULL,
    created_at timestamp without time zone,
    created_by character varying,
    modified_at timestamp without time zone,
    modified_by character varying,
    stock bigint DEFAULT 0 NOT NULL
);


ALTER TABLE public.products OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 32915)
-- Name: products_product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.products_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_product_id_seq OWNER TO postgres;

--
-- TOC entry 3960 (class 0 OID 0)
-- Dependencies: 203
-- Name: products_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.products_product_id_seq OWNED BY public.products.product_id;


--
-- TOC entry 199 (class 1259 OID 32885)
-- Name: transactions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transactions (
    trx_id bigint NOT NULL,
    product_id character varying,
    stock character varying,
    created_at timestamp without time zone,
    modified_at timestamp without time zone,
    payment_response character varying,
    transaction_status character varying,
    user_id bigint
);


ALTER TABLE public.transactions OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 32883)
-- Name: transaction_trx_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transaction_trx_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transaction_trx_id_seq OWNER TO postgres;

--
-- TOC entry 3961 (class 0 OID 0)
-- Dependencies: 198
-- Name: transaction_trx_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.transaction_trx_id_seq OWNED BY public.transactions.trx_id;


--
-- TOC entry 197 (class 1259 OID 28723)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    username character varying(100) NOT NULL,
    password character varying(100) NOT NULL,
    full_name character varying(100) NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 28721)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- TOC entry 3962 (class 0 OID 0)
-- Dependencies: 196
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public.users.id;


--
-- TOC entry 3808 (class 2604 OID 32941)
-- Name: payment_method_profiles profile_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_method_profiles ALTER COLUMN profile_id SET DEFAULT nextval('public.payment_method_profiles_profile_id_seq'::regclass);


--
-- TOC entry 3805 (class 2604 OID 32896)
-- Name: payment_methods method_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_methods ALTER COLUMN method_id SET DEFAULT nextval('public.payment_method_method_id_seq'::regclass);


--
-- TOC entry 3806 (class 2604 OID 32917)
-- Name: products product_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products ALTER COLUMN product_id SET DEFAULT nextval('public.products_product_id_seq'::regclass);


--
-- TOC entry 3804 (class 2604 OID 32888)
-- Name: transactions trx_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transactions ALTER COLUMN trx_id SET DEFAULT nextval('public.transaction_trx_id_seq'::regclass);


--
-- TOC entry 3803 (class 2604 OID 28726)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- TOC entry 3951 (class 0 OID 32938)
-- Dependencies: 205
-- Data for Name: payment_method_profiles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.payment_method_profiles VALUES (1, 3, 1, '0115476117');


--
-- TOC entry 3947 (class 0 OID 32893)
-- Dependencies: 201
-- Data for Name: payment_methods; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.payment_methods VALUES (1, 'isaku');


--
-- TOC entry 3948 (class 0 OID 32907)
-- Dependencies: 202
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.products VALUES (3, 25000000.000, 'Barang 2', '2020-08-03 21:10:51.93752', 'go-test', '2020-08-03 23:00:24.436969', 'go-test', 7);


--
-- TOC entry 3945 (class 0 OID 32885)
-- Dependencies: 199
-- Data for Name: transactions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.transactions VALUES (5, '3', '1', '2020-08-03 23:00:24.435278', '2020-08-03 23:00:24.435279', '{
    "doPaymentResponse": {
        "clientId": "IDBNIdGVzdHRlc3Q=",
        "parameters": {
            "responseCode": "0001",
            "responseMessage": "Request has been processed successfully",
            "responseTimestamp": "2020-08-03T23:00:24.408Z",
            "debitAccountNo": "0115476117",
            "creditAccountNo": "115471119",
            "valueAmount": "25000000",
            "valueCurrency": "IDR",
            "bankReference": "1596470424408",
            "customerReference": "20200803230023999001"
        }
    }
}', 'PROCESSED', 3);


--
-- TOC entry 3943 (class 0 OID 28723)
-- Dependencies: 197
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users VALUES (3, 'rifkyaziz', '$2a$14$DP6wFKnoc.vO43AEgJu90OKBm7f4QjBdlOd80U5DrPeCRTHS6Qfj2', 'Rifky Aziz Fauzianto');
INSERT INTO public.users VALUES (5, 'rifkyaziz2', '$2a$10$FHwz3OoF3n72AEix3gA0QeIa2Gd.JLi7cJIaEI5llT7Q0B1pW15T6', 'Rifky');


--
-- TOC entry 3963 (class 0 OID 0)
-- Dependencies: 200
-- Name: payment_method_method_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.payment_method_method_id_seq', 1, true);


--
-- TOC entry 3964 (class 0 OID 0)
-- Dependencies: 204
-- Name: payment_method_profiles_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.payment_method_profiles_profile_id_seq', 1, true);


--
-- TOC entry 3965 (class 0 OID 0)
-- Dependencies: 203
-- Name: products_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.products_product_id_seq', 3, true);


--
-- TOC entry 3966 (class 0 OID 0)
-- Dependencies: 198
-- Name: transaction_trx_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transaction_trx_id_seq', 5, true);


--
-- TOC entry 3967 (class 0 OID 0)
-- Dependencies: 196
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 5, true);


--
-- TOC entry 3816 (class 2606 OID 32898)
-- Name: payment_methods payment_method_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_methods
    ADD CONSTRAINT payment_method_pk PRIMARY KEY (method_id);


--
-- TOC entry 3820 (class 2606 OID 32943)
-- Name: payment_method_profiles payment_method_profiles_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_method_profiles
    ADD CONSTRAINT payment_method_profiles_pk PRIMARY KEY (profile_id);


--
-- TOC entry 3818 (class 2606 OID 32922)
-- Name: products products_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pk PRIMARY KEY (product_id);


--
-- TOC entry 3814 (class 2606 OID 32890)
-- Name: transactions transaction_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT transaction_pk PRIMARY KEY (trx_id);


--
-- TOC entry 3810 (class 2606 OID 28731)
-- Name: users users_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pk PRIMARY KEY (id);


--
-- TOC entry 3812 (class 2606 OID 28733)
-- Name: users users_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_un UNIQUE (username);


-- Completed on 2020-08-03 23:07:09 WIB

--
-- PostgreSQL database dump complete
--

