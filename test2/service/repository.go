package service

import (
	"context"
	"github.com/jmoiron/sqlx"
	"gitlab.com/rifkyaziz/go-developer-test/models"
)

type UserRepository interface {
	CreateUser(params models.UserRequest) (result models.User, err error)
	FindUser(userID int64) (result models.User, err error)
	FindUserByUsername(username string) (result models.User, err error)
	ListUser() (result []models.User, err error)
	UpdateUser(userID int64, params models.UserRequest) (err error)
	DeleteUser(userID int64) (err error)
}

type ProductRepository interface {
	CreateProduct(params models.ProductRequest) (result models.Product, err error)
	FindProduct(productID int64) (result models.Product, err error)
	ListProduct() (result []models.Product, err error)
	UpdateProduct(productID int64, params models.ProductRequest) (err error)
	UpdateProductTx(tx *sqlx.Tx, productID int64, params models.ProductRequest) (err error)
	DeleteProduct(productID int64) (err error)
}

type PaymentRepository interface {
	FindUserPaymentMethod(userID int64, paymentMethod string) (result models.PaymentMethodProfile, err error)
	CreateTransaction(tx *sqlx.Tx, params models.Transaction) (result int64, err error)
}

type ISakuRESTRepository interface {
	H2HGetBalance(ctx context.Context, accountNo string) (result []byte, err error)
	H2HGetInHouseInquiry(ctx context.Context, accountNo string) (result []byte, err error)
	H2HDoPayment(ctx context.Context, accountNo string, amount float64) (result []byte, err error)
}

type SQLRepository interface {
	BeginTxx() (tx *sqlx.Tx, err error)
	Commit(tx *sqlx.Tx) (err error)
	Rollback(tx *sqlx.Tx) (err error)
}
