package models

import "github.com/shopspring/decimal"

type (
	User struct {
		ID       int64  `db:"id" json:"id"`
		Username string `db:"username" json:"username"`
		Password string `db:"password" json:"password"`
		FullName string `db:"full_name" json:"full_name"`
	}

	Product struct {
		ProductID int64           `db:"product_id" json:"product_id"`
		Price     decimal.Decimal `db:"price" json:"price"`
		Name      string          `db:"name" json:"name"`
		Stock     int64           `db:"stock" json:"stock"`
	}

	PaymentMethodProfile struct {
		UserID    int64  `db:"user_id" json:"user_id"`
		MethodID  int64  `db:"method_id" json:"method_id"`
		AccountNo string `db:"account_no" json:"account_no"`
	}
)
