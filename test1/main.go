package main

import (
	"flag"
	"fmt"
	"strconv"
	"strings"
)

func main() {
	solution := flag.String("solution", "", "")
	argument := flag.String("args", "", "")
	flag.Parse()
	var result interface{}
	switch *solution {
	case "solution1":
		var arg []int
		for _, s := range strings.Split(*argument, ",") {
			in, err := strconv.Atoi(s)
			if err != nil {
				return
			}
			arg = append(arg, in)
		}
		result = solution1(arg)
		break
	case "solution2":
		var arg []int
		for _, s := range strings.Split(*argument, ",") {
			in, err := strconv.Atoi(s)
			if err != nil {
				return
			}
			arg = append(arg, in)
		}
		result = solution2(arg)
		break
	case "solution3":
		var arg []int
		for _, s := range strings.Split(*argument, ",") {
			in, err := strconv.Atoi(s)
			if err != nil {
				return
			}
			arg = append(arg, in)
		}
		result = solution3(arg)
		break
	case "solution4":
		var arg []int
		for _, s := range strings.Split(*argument, ",") {
			in, err := strconv.Atoi(s)
			if err != nil {
				return
			}
			arg = append(arg, in)
		}
		resultSol4, product := solution4(arg)

		fmt.Println(resultSol4)
		fmt.Println(product)
		break
	}

	fmt.Println(result)
}
