package main

func solution3(arr []int) (result [][]int) {
	mapHolder := make(map[int]int)
	for i := range arr {
		mapHolder[i] = 0
		for j := range arr {
			_, ok := mapHolder[j]
			if i > 0 && ok && i != j {
				continue
			}
			var sub []int
			sub = append(sub, arr[i])
			sub = append(sub, arr[j])
			result = append(result, sub)
		}
	}

	return result
}