package postgres

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/rifkyaziz/go-developer-test/models"
	"gitlab.com/rifkyaziz/go-developer-test/service"
	"log"
)

type userRepository struct {
	DB *sqlx.DB
}

func NewUserRepository(db *sqlx.DB) service.UserRepository {
	return userRepository{db}
}

func (u userRepository) CreateUser(params models.UserRequest) (result models.User, err error) {
	q := `INSERT INTO users
		(username, "password", full_name)
		VALUES($1, $2, $3) RETURNING id, username, "password", full_name
	`
	if err := u.DB.QueryRowx(q, params.Username, params.Password, params.FullName).StructScan(&result); err != nil {
		return result, err
	}
	return result, err
}

func (u userRepository) FindUser(userID int64) (result models.User, err error) {
	if err = u.DB.QueryRowx(`SELECT id, username, "password", full_name FROM users WHERE id = $1`, userID).StructScan(&result); err != nil {
		log.Printf("error while query find user. cause %+v", err)
		return result, err
	}

	return result, nil
}

func (u userRepository) FindUserByUsername(username string) (result models.User, err error) {
	if err = u.DB.QueryRowx(`SELECT id, username, "password", full_name FROM users WHERE username = $1`, username).StructScan(&result); err != nil {
		log.Printf("error while query find user. cause %+v", err)
		return result, err
	}

	return result, nil
}

func (u userRepository) ListUser() (result []models.User, err error) {
	rows, err  := u.DB.Queryx(`SELECT id, username, "password", full_name FROM users`)

	if err != nil {
		log.Printf("error while query list user. cause %+v", err)
		return result, err
	}

	defer rows.Close()
	for rows.Next() {
		var (
			user models.User
		)

		if err = rows.StructScan(&user); err != nil {
			log.Printf("error while struct scan list user. cause %+v", err)
			return result, err
		}
		result = append(result, user)
	}

	return result, nil
}

func (u userRepository) UpdateUser(userID int64, params models.UserRequest) (err error) {
	rows, err := u.DB.Queryx(`UPDATE users
		SET username=$2, "password"=$3, full_name=$4
		WHERE id=$1
	`, userID, params.Username, params.Password, params.FullName)

	if err != nil {
		return err
	}

	defer rows.Close()
	return nil
}

func (u userRepository) DeleteUser(userID int64) (err error) {
	rows, err := u.DB.Queryx(`DELETE FROM users WHERE id=$1`, userID)
	if err != nil {
		return err
	}
	defer rows.Close()
	return nil
}