package http

import (
	"github.com/go-chi/chi"
	"gitlab.com/rifkyaziz/go-developer-test/service"
)

type Handler struct {
	userUseCase    service.UserUseCase
	paymentUseCase service.PaymentUseCase
	productUseCase service.ProductUseCase
}

func InitRoutes(r chi.Router, userUseCase service.UserUseCase, paymentUseCase service.PaymentUseCase, productUseCase service.ProductUseCase) {
	handler := &Handler{userUseCase, paymentUseCase, productUseCase}
	r.Group(func(r chi.Router) {
		r.Post("/login", handler.Login)
		r.Route("/users", func(r chi.Router) {
			r.Get("/", handler.ListUser)
			r.Post("/", handler.CreateUser)
			r.Get("/{userID}", handler.FindUser)
			r.Put("/{userID}", handler.UpdateUser)
			r.Delete("/{userID}", handler.DeleteUser)
		})
		r.Route("/products", func(r chi.Router) {
			r.Get("/", handler.ListProduct)
			r.Post("/", handler.CreateProduct)
			r.Get("/{productID}", handler.FindProduct)
			r.Put("/{productID}", handler.UpdateProduct)
			r.Delete("/{productID}", handler.DeleteProduct)
			r.Post("/{productID}/pay/{paymentMethod}", handler.PayProduct)
		})
	})
}