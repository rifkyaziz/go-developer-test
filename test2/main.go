package main

import (
	"gitlab.com/rifkyaziz/go-developer-test/config"
	"log"
	"net/http"
)

func main() {
	log.Fatal(http.ListenAndServe(":45005", config.Init()))
}
