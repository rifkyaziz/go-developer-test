package usecase

import (
	"errors"
	"fmt"
	"gitlab.com/rifkyaziz/go-developer-test/models"
	"gitlab.com/rifkyaziz/go-developer-test/service"
)

type productUseCase struct {
	productRepo service.ProductRepository
}

func NewProductUseCase(repo service.ProductRepository) service.ProductUseCase {
	return productUseCase{repo}
}

func (u productUseCase) CreateProduct(params models.ProductRequest) (result models.Product, err error) {
	return u.productRepo.CreateProduct(params)
}

func (u productUseCase) FindProduct(productID int64) (result models.Product, err error) {
	return u.productRepo.FindProduct(productID)
}

func (u productUseCase) ListProduct() (result []models.Product, err error) {
	return u.productRepo.ListProduct()
}

func (u productUseCase) UpdateProduct(productID int64, params models.ProductRequest) (err error) {
	product, err := u.productRepo.FindProduct(productID)
	if err != nil {
		return err
	}

	if product.ProductID == 0 {
		msg := fmt.Sprintf("user id %d is not available", productID)
		return errors.New(msg)
	}

	return u.productRepo.UpdateProduct(productID, params)
}

func (u productUseCase) DeleteProduct(productID int64) (err error) {
	return u.productRepo.DeleteProduct(productID)
}
