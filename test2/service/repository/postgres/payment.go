package postgres

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/rifkyaziz/go-developer-test/models"
	"gitlab.com/rifkyaziz/go-developer-test/service"
	"log"
	"time"
)

type paymentRepository struct {
	DB *sqlx.DB
}

func NewPaymentRepository(db *sqlx.DB) service.PaymentRepository {
	return paymentRepository{db}
}

func (repo paymentRepository) CreateTransaction(tx *sqlx.Tx, params models.Transaction) (result int64, err error) {
	if err = tx.QueryRowx(`INSERT INTO transactions
		(user_id, product_id, stock, created_at, modified_at, payment_response, transaction_status)
		VALUES($1,$2,$3,$4,$5,$6,$7)
		RETURNING trx_id
	`, params.UserID, params.ProductID, params.Stock, time.Now(), time.Now(), params.PaymentResponse, params.TransactionStatus).Scan(&result); err != nil {
		log.Printf("error while query create transaction. cause %+v", err)
		return result,err
	}

	return result,nil
}

func (repo paymentRepository) FindUserPaymentMethod(userID int64, paymentMethod string) (result models.PaymentMethodProfile, err error) {
	if err = repo.DB.QueryRowx(`SELECT user_id, pmp.method_id, account_no from payment_method_profiles pmp
		JOIN payment_methods pm on pmp.method_id = pm.method_id
		WHERE user_id = $1 AND pm.provider = $2`, userID, paymentMethod).StructScan(&result); err != nil {
		return result, err
	}

	return result, nil
}
