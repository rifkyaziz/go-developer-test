package usecase

import (
	"context"
	"database/sql"
	"encoding/json"
	"github.com/google/uuid"
	"github.com/shopspring/decimal"
	"gitlab.com/rifkyaziz/go-developer-test/models"
	"gitlab.com/rifkyaziz/go-developer-test/service"
)

type paymentUseCase struct {
	productRepo service.ProductRepository
	paymentRepo service.PaymentRepository
	isakuRestRepo service.ISakuRESTRepository
	sqlRepo service.SQLRepository
}

func NewPaymentUseCase(productRepo service.ProductRepository, paymentRepo service.PaymentRepository, isakuRestRepo service.ISakuRESTRepository, sqlRepo service.SQLRepository) service.PaymentUseCase {
	return paymentUseCase{productRepo, paymentRepo, isakuRestRepo, sqlRepo}
}

func (uc paymentUseCase) PayProduct(productID int64, params models.PayRequest) (result interface{}, err error) {
	product, err := uc.productRepo.FindProduct(productID)
	if err != nil {
		if err == sql.ErrNoRows {
			return result, models.ErrorProductNotFound
		}
		return result, err
	}

	if product.Stock < params.Stock {
		return result, models.ErrorProductStockNotAvailable
	}

	switch params.PaymentMethod {
	case string(models.PaymentMethodIsaku):
		ctx := context.WithValue(context.Background(), "pid", uuid.New().String())
		profile, err := uc.paymentRepo.FindUserPaymentMethod(params.UserID, params.PaymentMethod)
		if err != nil {
			if err == sql.ErrNoRows {
				return result, models.ErrorUserPaymentMethodNotFound
			}
			return result, err
		}

		amount := product.Price.Mul(decimal.NewFromInt(params.Stock))
		balance, err := uc.isakuRestRepo.H2HGetBalance(ctx, profile.AccountNo)
		if err != nil {
			return result, err
		}

		var r models.ISAKUGetBalance
		if err = json.Unmarshal(balance, &r); err != nil {
			return result, err
		}

		if amount.GreaterThan(r.GetBalanceResponse.Parameters.AccountBalance) {
			return result, models.ErrorInsufficientFund
		}
		tx, err := uc.sqlRepo.BeginTxx()
		if err != nil {
			return result, err
		}

		amountF64, _ := amount.Float64()
		deduct, err := uc.isakuRestRepo.H2HDoPayment(ctx, profile.AccountNo, amountF64)
		if err != nil {
			_, err = uc.paymentRepo.CreateTransaction(tx, models.Transaction{
				UserID:            params.UserID,
				ProductID:         productID,
				Stock:             params.Stock,
				PaymentResponse:   string(deduct),
				TransactionStatus: models.TransactionStatusFailed,
			})
			if err != nil {
				tx.Rollback()
				return result, models.ErrorCreateTransaction
			}

			return result, err
		}

		var paymentResponse models.ISAKUDoPaymentResponse
		if err = json.Unmarshal(deduct, &paymentResponse); err != nil {
			return result, err
		}

		_, err = uc.paymentRepo.CreateTransaction(tx, models.Transaction{
			UserID:            params.UserID,
			ProductID:         productID,
			Stock:             params.Stock,
			PaymentResponse:   string(deduct),
			TransactionStatus: models.TransactionStatusProcessed,
		})
		if err != nil {
			tx.Rollback()
			return result, models.ErrorCreateTransaction
		}

		if err = uc.productRepo.UpdateProduct(productID, models.ProductRequest{
			Price: product.Price,
			Name:  product.Name,
			Stock: product.Stock-params.Stock,
		}); err != nil {
			tx.Rollback()
			return result, err
		}

		tx.Commit()
		break
	default:
		return result, models.ErrorPaymentMethodNotSupported
	}

	return result, nil
}
