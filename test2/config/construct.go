package config

import (
	"github.com/go-chi/chi"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	"log"
)

type Config struct {
	Router    *chi.Mux
	RouteList chi.Router
	configMap map[string]string
	DB *sqlx.DB
}

func Init() *chi.Mux {
	var c Config
	catch(c.InitEnv())
	catch(c.InitPostgres())
	c.InitRouter()
	c.InitService()

	log.Print("Starting server...")
	return c.Router
}

func (c *Config) InitEnv() error {
	env, err := godotenv.Read()
	if err != nil {
		log.Fatalf("error read .env. cause %+v", err)
		return err
	}

	c.configMap = env
	return nil
}

func catch(err error)  {
	if err != nil {
		log.Panic(err)
	}
}