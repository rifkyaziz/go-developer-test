package main

import (
	"sort"
)

//func main() {
//	r := solution([]int{1,2,3,4,5,6,7,8,2})
//	fmt.Print(r)
//}

func solution1(arr []int) (result int) {
	j := len(arr)-1
	sort.Ints(arr)
	for i := range arr {
		if i > j {
			break
		}
		if arr[i] == arr[j] {
			result = arr[i]
			break
		}
		if i > 0 && arr[i] == arr[i-1] {
			result = arr[i]
			break
		}
		if j > 0 && arr[j] == arr[j-1] {
			result = arr[j]
			break
		}
		j--
	}

	return result
}