package postgres

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/rifkyaziz/go-developer-test/models"
	"gitlab.com/rifkyaziz/go-developer-test/service"
	"log"
	"time"
)

type productRepository struct {
	DB *sqlx.DB
}

func NewProductRepository(db *sqlx.DB) service.ProductRepository {
	return productRepository{db}
}

func (p productRepository) CreateProduct(params models.ProductRequest) (result models.Product, err error) {
	q := `INSERT INTO products
		(name, price, stock, created_at, created_by, modified_at, modified_by)
		VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING product_id, name, price, stock
	`
	if err := p.DB.QueryRowx(q, params.Name, params.Price, params.Stock, time.Now(), "go-test", time.Now(), "go-test").StructScan(&result); err != nil {
		return result, err
	}
	return result, err
}

func (p productRepository) FindProduct(productID int64) (result models.Product, err error) {
	if err = p.DB.QueryRowx(`SELECT product_id, name, price, stock FROM products WHERE product_id = $1`, productID).StructScan(&result); err != nil {
		log.Printf("error while query find user. cause %+v", err)
		return result, err
	}

	return result, nil
}

func (p productRepository) ListProduct() (result []models.Product, err error) {
	rows, err  := p.DB.Queryx(`SELECT product_id, name, price, stock FROM products`)

	if err != nil {
		log.Printf("error while query list product. cause %+v", err)
		return result, err
	}

	defer rows.Close()
	for rows.Next() {
		var (
			product models.Product
		)

		if err = rows.StructScan(&product); err != nil {
			log.Printf("error while struct scan list product. cause %+v", err)
			return result, err
		}
		result = append(result, product)
	}

	return result, nil
}

func (p productRepository) UpdateProduct(productID int64, params models.ProductRequest) (err error) {
	rows, err := p.DB.Queryx(`UPDATE products
		SET name=$2, price=$3, stock=$4, modified_at = $5, modified_by = $6
		WHERE product_id=$1
	`, productID, params.Name, params.Price, params.Stock, time.Now(), "go-test")

	if err != nil {
		return err
	}

	defer rows.Close()
	return nil
}

func (p productRepository) UpdateProductTx(tx *sqlx.Tx, productID int64, params models.ProductRequest) (err error) {
	rows, err := tx.Queryx(`UPDATE products
		SET name=$2, price=$3, stock=$4, modified_at = $5, modified_by = $6
		WHERE product_id=$1
	`, productID, params.Name, params.Price, params.Stock, time.Now(), "go-test")

	if err != nil {
		return err
	}

	defer rows.Close()
	return nil
}

func (p productRepository) DeleteProduct(productID int64) (err error) {
	rows, err := p.DB.Queryx(`DELETE FROM products WHERE product_id=$1`, productID)
	if err != nil {
		return err
	}
	defer rows.Close()
	return nil
}