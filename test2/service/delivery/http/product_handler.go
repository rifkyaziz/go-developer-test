package http

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/rifkyaziz/go-developer-test/models"
	"gopkg.in/validator.v2"
	"net/http"
)

func (h *Handler) ListProduct(w http.ResponseWriter, r *http.Request) {
	data, err := h.productUseCase.ListProduct()
	if err != nil {
		throwServerError(w, http.StatusInternalServerError, err)
		return
	}
	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: http.StatusOK,
		Data: data,
	})
}

func (h *Handler) CreateProduct(w http.ResponseWriter, r *http.Request) {
	var productReq models.ProductRequest
	if err := json.NewDecoder(r.Body).Decode(&productReq); err != nil {
		throwServerError(w, http.StatusBadRequest, err)
		return
	}

	if err := validator.Validate(productReq); err != nil {
		throwServerError(w, http.StatusBadRequest, err)
		return
	}

	data, err := h.productUseCase.CreateProduct(productReq)
	if err != nil {
		throwServerError(w, http.StatusInternalServerError, err)
		return
	}
	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: http.StatusOK,
		Data: data,
	})
}

func (h *Handler) FindProduct(w http.ResponseWriter, r *http.Request) {
	productID := stringToInt64(chi.URLParam(r, "productID"), 0)
	if productID == 0 {
		throwServerError(w, http.StatusNotFound, models.ErrorProductNotFound)
	}
	data, err := h.productUseCase.FindProduct(productID)
	if err != nil {
		throwServerError(w, http.StatusInternalServerError, err)
		return
	}
	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: http.StatusOK,
		Data: data,
	})
}

func (h *Handler) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	productID := stringToInt64(chi.URLParam(r, "productID"), 0)
	if productID == 0 {
		throwServerError(w, http.StatusNotFound, models.ErrorProductNotFound)
	}

	var productReq models.ProductRequest
	if err := json.NewDecoder(r.Body).Decode(&productReq); err != nil {
		throwServerError(w, http.StatusBadRequest, err)
		return
	}

	if err := validator.Validate(productReq); err != nil {
		throwServerError(w, http.StatusBadRequest, err)
		return
	}

	err := h.productUseCase.UpdateProduct(productID, productReq)
	if err != nil {
		throwServerError(w, http.StatusInternalServerError, err)
		return
	}
	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: http.StatusOK,
		Data: nil,
	})
}

func (h *Handler) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	productID := stringToInt64(chi.URLParam(r, "productID"), 0)
	if productID == 0 {
		throwServerError(w, http.StatusNotFound, models.ErrorProductNotFound)
	}

	err := h.productUseCase.DeleteProduct(productID)
	if err != nil {
		throwServerError(w, http.StatusInternalServerError, err)
		return
	}
	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: http.StatusOK,
		Data: nil,
	})
}

func (h *Handler) PayProduct(w http.ResponseWriter, r *http.Request) {
	productID := stringToInt64(chi.URLParam(r, "productID"), 0)
	if productID == 0 {
		throwServerError(w, http.StatusNotFound, models.ErrorProductNotFound)
	}

	paymentMethod := chi.URLParam(r, "paymentMethod")
	var payRequest models.PayRequest
	if err := json.NewDecoder(r.Body).Decode(&payRequest); err != nil {
		throwServerError(w, http.StatusBadRequest, err)
		return
	}
	payRequest.PaymentMethod = paymentMethod
	if err := validator.Validate(payRequest); err != nil {
		throwServerError(w, http.StatusBadRequest, err)
		return
	}

	data, err := h.paymentUseCase.PayProduct(productID, payRequest)
	if err != nil {
		throwServerError(w, http.StatusInternalServerError, err)
		return
	}

	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: http.StatusOK,
		Data: data,
	})
}
