package config

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/jwtauth"
	"time"
)

func (c *Config) InitRouter() *chi.Mux {
	c.Router = chi.NewRouter()

	jwtSigningKey := []byte(c.configMap["JWT_SECRET_KEY"])
	jwt := jwtauth.New("HS256", jwtSigningKey, nil)
	c.Router.Use(jwtauth.Verifier(jwt))
	c.Router.Use(middleware.RequestID)
	c.Router.Use(middleware.RealIP)
	c.Router.Use(middleware.Logger)
	c.Router.Use(middleware.Recoverer)
	c.Router.Use(middleware.Timeout(60 * time.Second))
	c.Router.Use(middleware.SetHeader("Content-Type", "application/json"))

	c.RouteList = c.Router.Route("/", func(r chi.Router) {})

	return c.Router
}
