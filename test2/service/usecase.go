package service

import "gitlab.com/rifkyaziz/go-developer-test/models"

type UserUseCase interface {
	CreateUser(params models.UserRequest) (result models.User, err error)
	FindUser(userID int64) (result models.User, err error)
	ListUser() (result []models.User, err error)
	UpdateUser(userID int64, params models.UserRequest) (err error)
	DeleteUser(userID int64) (err error)
	Login(params models.AuthRequest) (result models.AuthResponse, err error)
}

type ProductUseCase interface {
	CreateProduct(params models.ProductRequest) (result models.Product, err error)
	FindProduct(userID int64) (result models.Product, err error)
	ListProduct() (result []models.Product, err error)
	UpdateProduct(userID int64, params models.ProductRequest) (err error)
	DeleteProduct(userID int64) (err error)
}

type PaymentUseCase interface {
	PayProduct(productID int64, params models.PayRequest) (result interface{}, err error)
}
