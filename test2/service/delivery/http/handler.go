package http

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/rifkyaziz/go-developer-test/models"
	"gopkg.in/validator.v2"
	"net/http"
	"strconv"
)

func throwServerError(w http.ResponseWriter, httpStatus int, err error) {
	w.WriteHeader(httpStatus)
	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: httpStatus,
		Data: map[string]interface{}{
			"message": err.Error(),
		},
	})
}

func stringToInt64(value string, def int64) (result int64) {
	result, err := strconv.ParseInt(value, 10, 64)
	if err != nil {
		result = def
	}

	return result
}

func (h *Handler) ListUser(w http.ResponseWriter, r *http.Request) {
	data, err := h.userUseCase.ListUser()
	if err != nil {
		throwServerError(w, http.StatusInternalServerError, err)
		return
	}
	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: http.StatusOK,
		Data: data,
	})
}

func (h *Handler) CreateUser(w http.ResponseWriter, r *http.Request) {
	var userReq models.UserRequest
	if err := json.NewDecoder(r.Body).Decode(&userReq); err != nil {
		throwServerError(w, http.StatusBadRequest, err)
		return
	}

	if err := validator.Validate(userReq); err != nil {
		throwServerError(w, http.StatusBadRequest, err)
		return
	}

	data, err := h.userUseCase.CreateUser(userReq)
	if err != nil {
		throwServerError(w, http.StatusInternalServerError, err)
		return
	}
	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: http.StatusOK,
		Data: data,
	})
}

func (h *Handler) FindUser(w http.ResponseWriter, r *http.Request) {
	userID := stringToInt64(chi.URLParam(r, "userID"), 0)
	data, err := h.userUseCase.FindUser(userID)
	if err != nil {
		throwServerError(w, http.StatusInternalServerError, err)
		return
	}
	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: http.StatusOK,
		Data: data,
	})
}

func (h *Handler) UpdateUser(w http.ResponseWriter, r *http.Request) {
	userID := stringToInt64(chi.URLParam(r, "userID"), 0)
	var userReq models.UserRequest
	if err := json.NewDecoder(r.Body).Decode(&userReq); err != nil {
		throwServerError(w, http.StatusBadRequest, err)
		return
	}

	if err := validator.Validate(userReq); err != nil {
		throwServerError(w, http.StatusBadRequest, err)
		return
	}

	err := h.userUseCase.UpdateUser(userID, userReq)
	if err != nil {
		throwServerError(w, http.StatusInternalServerError, err)
		return
	}
	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: http.StatusOK,
		Data: nil,
	})
}

func (h *Handler) DeleteUser(w http.ResponseWriter, r *http.Request) {
	userID := stringToInt64(chi.URLParam(r, "userID"), 0)
	err := h.userUseCase.DeleteUser(userID)
	if err != nil {
		throwServerError(w, http.StatusInternalServerError, err)
		return
	}
	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: http.StatusOK,
		Data: nil,
	})
}

func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	var authReq models.AuthRequest
	if err := json.NewDecoder(r.Body).Decode(&authReq); err != nil {
		throwServerError(w, http.StatusBadRequest, err)
		return
	}

	if err := validator.Validate(authReq); err != nil {
		throwServerError(w, http.StatusBadRequest, err)
		return
	}

	data, err := h.userUseCase.Login(authReq)
	if err != nil {
		throwServerError(w, http.StatusInternalServerError, err)
		return
	}
	json.NewEncoder(w).Encode(models.JSONResponse{
		Code: http.StatusOK,
		Data: data,
	})
}