package config

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func (c *Config) InitPostgres() (err error) {
	db, err := sqlx.Connect("postgres", c.configMap["PG_CONF"])
	if err != nil {
		return err
	}
	c.DB = db
	return nil
}
