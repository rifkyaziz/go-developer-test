package models

import (
	"errors"
)

type (
	PaymentMethod string
	Transaction struct {
		UserID int64
		ProductID int64
		Stock int64
		PaymentResponse string
		TransactionStatus string
	}
)

const (
	PaymentMethodIsaku PaymentMethod = "isaku"
	TransactionStatusProcessed = "PROCESSED"
	TransactionStatusFailed = "FAILED"
)

var (
	ErrorPaymentMethodNotSupported = errors.New("payment method not supported")
	ErrorUserPaymentMethodNotFound = errors.New("user payment method not found")
	ErrorInsufficientFund = errors.New("insufficient fund")
	ErrorCreateTransaction = errors.New("failed create transaction")
)
