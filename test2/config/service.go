package config

import (
	"gitlab.com/rifkyaziz/go-developer-test/service/delivery/http"
	"gitlab.com/rifkyaziz/go-developer-test/service/repository/postgres"
	"gitlab.com/rifkyaziz/go-developer-test/service/repository/rest"
	"gitlab.com/rifkyaziz/go-developer-test/service/usecase"
)

func (c *Config) InitService() {
	userRepo := postgres.NewUserRepository(c.DB)
	paymentRepo := postgres.NewPaymentRepository(c.DB)
	productRepo := postgres.NewProductRepository(c.DB)
	sqlRepo := postgres.NewSQLRepository(c.DB)
	isakuRestRepo := rest.NewISakuRESTRepository(c.configMap["ISAKU_HOST"], c.configMap["ISAKU_API_KEY"], c.configMap["ISAKU_API_SECRET_KEY"],c.configMap["ISAKU_CLIENT_NAME"], c.configMap["ISAKU_OAUTH_KEY"], c.configMap["ISAKU_OAUTH_SECRET"])

	userUseCase := usecase.NewUserUseCase(userRepo, c.configMap)
	paymentUseCase := usecase.NewPaymentUseCase(productRepo, paymentRepo, isakuRestRepo, sqlRepo)
	productUseCase := usecase.NewProductUseCase(productRepo)
	http.InitRoutes(c.RouteList, userUseCase, paymentUseCase, productUseCase)
}