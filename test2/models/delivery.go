package models

import "github.com/shopspring/decimal"

type JSONResponse struct {
	Code int         `json:"code"`
	Data interface{} `json:"data"`
}

type UserRequest struct {
	Username string `json:"username" validate:"nonzero, nonnil" `
	Password string `json:"password" validate:"nonzero, nonnil"`
	FullName string `json:"full_name" validate:"nonzero, nonnil"`
}

type AuthRequest struct {
	Username string `json:"username" validate:"nonzero, nonnil" `
	Password string `json:"password" validate:"nonzero, nonnil"`
}

type AuthResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type (
	PayRequest struct {
		UserID        int64           `json:"user_id" validate:"nonzero, nonnil"`
		PaymentMethod string          `json:"payment_method"`
		Stock         int64           `json:"stock" validate:"nonzero, nonnil"`
	}

	ProductRequest struct {
		Price decimal.Decimal `json:"price" validate:"nonzero, nonnil" `
		Name  string          `json:"name" validate:"nonzero, nonnil"`
		Stock int64           `json:"stock" validate:"nonzero, nonnil"`
	}
)
