package models

import (
	"github.com/shopspring/decimal"
	"time"
)

const (
	URLH2HGetBalance string = "H2H/v2/getbalance"
	URLH2HGetInHouseInquiry string = "H2H/v2/getinhouseinquiry"
	URLH2HDoPayment string = "H2H/v2/dopayment"
)

type (
	ISAKUToken struct {
		AccessToken string
		ExpiresIn   time.Time
	}

	ISAKUGetToken struct {
		AccessToken string `json:"access_token"`
		TokenType   string `json:"token_type"`
		ExpiresIn   int64  `json:"expires_in"`
		Scope       string `json:"scope"`
	}

	ISAKUGetBalance struct {
		GetBalanceResponse struct {
			ClientID string `json:"clientId"`
			Parameters struct {
				ResponseCode string `json:"responseCode"`
				ResponseMessage string `json:"responseMessage"`
				ResponseTimestamp string `json:"responseTimestamp"`
				CustomerName string `json:"customerName"`
				AccountCurrency string `json:"accountCurrency"`
				AccountBalance decimal.Decimal `json:"accountBalance"`
			} `json:"parameters"`
		} `json:"getBalanceResponse"`
	}

	ISAKUGetInHouseInquiryResponse struct {
		GetInHouseInquiryResponse struct {
			ClientID string `json:"clientId"`
			Parameters struct {
				ResponseCode string `json:"responseCode"`
				ResponseMessage string `json:"responseMessage"`
				ResponseTimestamp string `json:"responseTimestamp"`
				CustomerName string `json:"customerName"`
				AccountCurrency string `json:"accountCurrency"`
				AccountNumber string `json:"accountNumber"`
				AccountStatus string `json:"accountStatus"`
				AccountType string `json:"accountType"`
			} `json:"parameters"`
		} `json:"getInHouseInquiryResponse"`
	}

	ISAKUDoPaymentResponse struct {
		DoPaymentResponse struct {
			ClientID string `json:"clientId"`
			Parameters struct {
				ResponseCode string `json:"responseCode"`
				ResponseMessage string `json:"responseMessage"`
				ResponseTimestamp string `json:"responseTimestamp"`
				DebitAccountNo string `json:"debitAccountNo"`
				CreditAccountNo string `json:"creditAccountNo"`
				ValueAmount string `json:"valueAmount"`
				ValueCurrency string `json:"valueCurrency"`
				BankReference string `json:"bankReference"`
				CustomerReference string `json:"customerReference"`
			} `json:"parameters"`
		} `json:"doPaymentResponse"`
	}
)
