package rest

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/rifkyaziz/go-developer-test/models"
	"gitlab.com/rifkyaziz/go-developer-test/service"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type isakuRESTRepository struct {
	Host        string
	APIKey      string
	APISecretKey      string
	ClientName  string
	OauthKey    string
	OauthSecret string
}

var iSAKUToken models.ISAKUToken
var (
	ErrNotOKResponse = errors.New("not 200 response code")
)

func NewISakuRESTRepository(host, apiKey, apiSecretKey, clientName, oauthKey, oauthSecret string) service.ISakuRESTRepository {
	return isakuRESTRepository{
		Host:        host,
		APIKey:      apiKey,
		APISecretKey:      apiSecretKey,
		ClientName:  clientName,
		OauthKey:    oauthKey,
		OauthSecret: oauthSecret,
	}
}

func (repo isakuRESTRepository) NewClient() *http.Client {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	return &http.Client{
		Transport: tr,
	}
}

func (repo isakuRESTRepository) NewBasicAuth() string {
	return base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", repo.OauthKey, repo.OauthSecret)))
}

func (repo isakuRESTRepository) NewClientID() string {
	return fmt.Sprintf("IDBNI%s", base64.URLEncoding.EncodeToString([]byte(repo.ClientName)))
}

func (repo isakuRESTRepository) NewSignature(body interface{}) (result string, err error) {
	b, err := json.Marshal(body)
	if err != nil {
		log.Printf("%+v", err)
		return result, err
	}
	log.Printf("%s", string(b))
	jwtPayload := base64.URLEncoding.WithPadding(base64.NoPadding).EncodeToString(b)

	const jwtHeader = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
	data := jwtHeader + "." + jwtPayload
	h := hmac.New(sha256.New, []byte(repo.APISecretKey))
	h.Write([]byte(data))
	result = fmt.Sprintf("%s.%s", data, base64.RawURLEncoding.EncodeToString(h.Sum(nil)))
	return result, nil
}

func (repo isakuRESTRepository) NewReq(tokenFuture func() (models.ISAKUToken, error), fullUrl string, body []byte) (result *http.Request, err error) {
	result, err = http.NewRequest("POST", fullUrl, bytes.NewBuffer(body))
	if err != nil {
		log.Fatalf("[NewReq] %+v", err)
		return result, err
	}

	token, err := tokenFuture()
	if err != nil {
		log.Fatalf("[NewReq] %+v", err)
		return result, err
	}

	q := result.URL.Query()
	q.Add("access_token", token.AccessToken)

	result.Header.Add("Content-Type", "application/json")
	result.Header.Add("x-api-key", repo.APIKey)
	result.URL.RawQuery = q.Encode()

	return result, nil
}

func (repo isakuRESTRepository) GetTokenFuture(ctx context.Context) func() (models.ISAKUToken, error) {
	var result models.ISAKUToken
	var err error
	c := make(chan struct{}, 1)
	if iSAKUToken.AccessToken != "" && iSAKUToken.ExpiresIn.Before(time.Now()) {
		result = iSAKUToken
		return func() (models.ISAKUToken, error) {
			<-c
			return result, err
		}
	}

	go func() {
		defer close(c)
		client := repo.NewClient()
		basicAuth := repo.NewBasicAuth()
		fullUrl := fmt.Sprintf("%s/%s", repo.Host, "api/oauth/token")
		requestBody := url.Values{}
		requestBody.Set("grant_type", "client_credentials")
		req, err := http.NewRequest("POST", fullUrl, strings.NewReader(requestBody.Encode()))
		if err != nil {
			log.Printf("[processID:%v][GetTokenFuture] while init request: %+v", ctx.Value("pid"), err)
			return
		}

		req.Header.Add("Authorization", "Basic "+basicAuth)
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

		resp, err := client.Do(req)
		if err != nil {
			log.Printf("[processID:%v][GetTokenFuture] while sending request: %+v", ctx.Value("pid"), err)
			return
		}

		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Printf("[processID:%v][GetTokenFuture] while read response: %+v", ctx.Value("pid"), err)
			return
		}

		if resp.StatusCode != http.StatusOK {
			log.Printf("[processID:%v][GetTokenFuture] response code is not 200(%d): %+v", ctx.Value("pid"), resp.StatusCode, string(body))
			err = ErrNotOKResponse
			return
		}

		var response models.ISAKUGetToken
		if err = json.Unmarshal(body, &response); err != nil {
			log.Printf("[processID:%v][GetTokenFuture] while parsing response: %+v", ctx.Value("pid"), err)
			return
		}

		result = models.ISAKUToken{
			AccessToken: response.AccessToken,
			ExpiresIn:   time.Now().Add(time.Duration(response.ExpiresIn) * time.Second),
		}
	}()

	return func() (models.ISAKUToken, error) {
		<-c
		return result, err
	}
}

func (repo isakuRESTRepository) H2HGetBalance(ctx context.Context, accountNo string) (result []byte, err error) {
	type Body struct {
		ClientID  string `json:"clientId"`
		AccountNo string `json:"accountNo"`
		Signature string `json:"signature,omitempty"`
	}

	token := repo.GetTokenFuture(ctx)
	body := Body{
		ClientID:  repo.NewClientID(),
		AccountNo: accountNo,
	}
	signature, err := repo.NewSignature(Body{
		ClientID:  repo.NewClientID(),
		AccountNo: accountNo,
	})
	if err != nil {
		log.Printf("[processID:%v][H2HGetBalance] while generate new signature: %+v", ctx.Value("pid"), err)
		return result, err
	}
	body.Signature = signature

	client := repo.NewClient()
	fullUrl := fmt.Sprintf("%s/%s", repo.Host, models.URLH2HGetBalance)
	reqBody, err := json.Marshal(body)
	if err != nil {
		log.Printf("[processID:%v][H2HGetBalance] while generate new signature: %+v", ctx.Value("pid"), err)
		return result, err
	}

	req, err := repo.NewReq(token, fullUrl, reqBody)
	if err != nil {
		log.Printf("[processID:%v][H2HGetBalance] while init request: %+v", ctx.Value("pid"), err)
		return result, err
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("[processID:%v][H2HGetBalance] while sending request: %+v", ctx.Value("pid"), err)
		return result, err
	}

	result, _ = ioutil.ReadAll(resp.Body)
	if resp.StatusCode != http.StatusOK {
		log.Printf("[processID:%v][H2HGetBalance] response code is not 200(%d): %+v", ctx.Value("pid"), resp.StatusCode, string(result))
		return result, ErrNotOKResponse
	}

	return result, nil
}

func (repo isakuRESTRepository) H2HGetInHouseInquiry(ctx context.Context, accountNo string) (result []byte, err error) {
	type Body struct {
		ClientID  string `json:"clientId"`
		AccountNo string `json:"accountNo"`
		Signature string `json:"signature,omitempty"`
	}

	token := repo.GetTokenFuture(ctx)
	body := Body{
		ClientID:  repo.NewClientID(),
		AccountNo: accountNo,
	}
	signature, err := repo.NewSignature(body)
	if err != nil {
		log.Printf("[processID:%v][H2HGetInHouseInquiry] while generate new signature: %+v", ctx.Value("pid"), err)
		return result, err
	}
	body.Signature = signature

	client := repo.NewClient()
	fullUrl := fmt.Sprintf("%s/%s", repo.Host, models.URLH2HGetInHouseInquiry)
	reqBody, err := json.Marshal(body)
	if err != nil {
		log.Printf("[processID:%v][H2HGetInHouseInquiry generate new signature: %+v", ctx.Value("pid"), err)
		return result, err
	}

	req, err := repo.NewReq(token, fullUrl, reqBody)
	if err != nil {
		log.Printf("[processID:%v][H2HGetInHouseInquiry] while init request: %+v", ctx.Value("pid"), err)
		return result, err
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("[processID:%v][H2HGetInHouseInquiry] while sending request: %+v", ctx.Value("pid"), err)
		return result, err
	}

	result, _ = ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		log.Printf("[processID:%v][H2HGetInHouseInquiry] response code is not 200(%d): %+v", ctx.Value("pid"), resp.StatusCode, string(result))
		return result, ErrNotOKResponse
	}

	log.Printf("response: %s", result)
	var s models.ISAKUGetInHouseInquiryResponse
	if err = json.Unmarshal(result, &s); err != nil {
		return result, err
	}
	log.Printf("response struct: %+v", s)
	return result, nil
}

func (repo isakuRESTRepository) H2HDoPayment(ctx context.Context, accountNo string, amount float64) (result []byte, err error) {
	type Body struct {
		ClientID  string `json:"clientId"`
		Signature string `json:"signature,omitempty"`
		CustomerReferenceNumber string `json:"customerReferenceNumber"`
		PaymentMethod string `json:"paymentMethod"`
		DebitAccountNo string `json:"debitAccountNo"`
		CreditAccountNo string `json:"creditAccountNo"`
		ValueDate string `json:"valueDate"`
		ValueCurrency string `json:"valueCurrency"`
		ValueAmount float64 `json:"valueAmount"`
	}

	valueDate := time.Now().Format("20060102150405")
	referenceNo := time.Now().Format("20060102150405999")
	token := repo.GetTokenFuture(ctx)
	body := Body{
		ClientID:  repo.NewClientID(),
		DebitAccountNo: accountNo,
		CustomerReferenceNumber: referenceNo+"001",
		PaymentMethod: "0",
		CreditAccountNo: "115471119",
		ValueDate: valueDate,
		ValueCurrency: "IDR",
		ValueAmount: amount,
	}
	signature, err := repo.NewSignature(body)
	if err != nil {
		log.Printf("[processID:%v][H2HDoPayment] while generate new signature: %+v", ctx.Value("pid"), err)
		return result, err
	}
	body.Signature = signature

	client := repo.NewClient()
	fullUrl := fmt.Sprintf("%s/%s", repo.Host, models.URLH2HDoPayment)
	reqBody, err := json.Marshal(body)
	if err != nil {
		log.Printf("[processID:%v][H2HDoPayment] generate new signature: %+v", ctx.Value("pid"), err)
		return result, err
	}

	req, err := repo.NewReq(token, fullUrl, reqBody)
	if err != nil {
		log.Printf("[processID:%v][H2HDoPayment] while init request: %+v", ctx.Value("pid"), err)
		return result, err
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("[processID:%v][H2HDoPayment] while sending request: %+v", ctx.Value("pid"), err)
		return result, err
	}

	result, _ = ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		log.Printf("[processID:%v][H2HDoPayment] response code is not 200(%d): %+v", ctx.Value("pid"), resp.StatusCode, string(result))
		return result, ErrNotOKResponse
	}

	return result, nil
}

